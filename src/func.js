const getSum = (str1, str2) => {
  if (isNaN(str1) || isNaN(str2)) {
    return false;
  }

  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  }

  let result = BigInt(str1) + BigInt(str2);
  return result.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let numOfPost = 0;
  let numOfComment = 0;
  for (let post of listOfPosts) {
    if (post['author'] === authorName) {
      numOfPost++;
    }

    for (let i = 0; i < post['comments']?.length; i++) {
      if (post['comments']) {
        if (post['comments'][i].author === authorName) {
          numOfComment++;
        }
      }
    }
  }
  return `Post:${numOfPost},comments:${numOfComment}`;
};

const tickets = (people) => {
  let change = 0;
  const ticketPrice = 25;

  for (let pay of people) {
    let needChange = +pay - ticketPrice;

    if (needChange !== 0) {
      if (change - needChange < 0) {
        return 'NO';
      }
    }
    change = change - needChange + pay;
  }
  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
